'use strict';

const baseURL = "https://li-ext-server.herokuapp.com/api/v1";

const REQUEST_GET_PROFILE = "get_profile";
const REQUEST_SAVE_PROFILE = "save_profile";
const REQUEST_SAVE_TAG = "save_tag";
const REQUEST_DELETE_TAG = "delete_tag";

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    switch (message.method) {
        case REQUEST_GET_PROFILE:
            GET(
                `${baseURL}/profiles/${encodeURIComponent(message.id)}`,
                (response) => { sendResponse({type: REQUEST_GET_PROFILE, success: true, profile: JSON.parse(response)}) },
                errorHandler(sendResponse, REQUEST_GET_PROFILE)
            );
            break;

        case REQUEST_SAVE_PROFILE:
            POST(
                `${baseURL}/profiles`,
                message.profile,
                () => { sendResponse({type: REQUEST_SAVE_PROFILE, success: true}) },
                errorHandler(sendResponse, REQUEST_SAVE_PROFILE)
            );
            break;

        case REQUEST_SAVE_TAG:
            POST(
                `${baseURL}/profiles/${encodeURIComponent(message.id)}/tag/${encodeURIComponent(message.tag)}`,
                {},
                () => { sendResponse({type: REQUEST_SAVE_TAG, success: true}) },
                errorHandler(sendResponse, REQUEST_SAVE_TAG)
            );
            break;

        case REQUEST_DELETE_TAG:
            DELETE(
                `${baseURL}/profiles/${encodeURIComponent(message.id)}/tag/${encodeURIComponent(message.tag)}`,
                {},
                () => { sendResponse({type: REQUEST_DELETE_TAG, success: true}) },
                errorHandler(sendResponse, REQUEST_DELETE_TAG)
            );
            break;

        default:
            return false;
    }

    // This function becomes invalid when the event listener returns,
    // unless you return true from the event listener to indicate you wish to send a response asynchronously
    // (this will keep the message channel open to the other end until sendResponse is called).
    return true;
});

function errorHandler(sendResponse, type) {
    return (code, message) => {
        sendResponse({type: type, success: false, error_code: code, error_message: message});
    }
}

function GET(url, onSuccess, onError) {
    sendRequest('GET', url, null, onSuccess, onError)
}

function POST(url, payload, onSuccess, onError) {
    sendRequest('POST', url, payload, onSuccess, onError)
}

function DELETE(url, onSuccess, onError) {
    sendRequest('DELETE', url, onSuccess, onError)
}

function sendRequest(method, url, payload, onSuccess, onError) {
    let xhr = new XMLHttpRequest();
    xhr.open(method, url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onreadystatechange = function() {
        if (xhr.readyState !== 4) {
            return;
        }

        if (xhr.status - 200 < 100) {
            onSuccess(this.responseText);
        } else {
            console.error(`Error during sending request. Code: ${xhr.status}. Message: ${xhr.responseText}`);
            onError(xhr.status, xhr.responseText);
        }
    };

    let request = payload ? JSON.stringify(payload) : undefined;
    xhr.send(request);
}
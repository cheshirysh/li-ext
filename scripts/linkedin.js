'use strict';

$(function() {
    const PARENT = ".pv-top-card-v2-section__info";

    const CONTAINER_ID = "li-ext-container";
    const ADD_BUTTON_ID = "li-ext-button";
    const COMMENT_BUTTON_ID = "li-ext-button-comment";
    const COMMENT_CONTAINER_ID = "li-ext-comment-container";
    const TAGS_CONTAINER_ID = "li-ext-tags-container";
    const TAGS_ID = "li-ext-tags";

    const CLASS_ICON = "li-ext-icon";
    const CLASS_ICON_ADD = "li-ext-add";
    const CLASS_ICON_ADDED = "li-ext-added";
    const CLASS_ICON_IN_PROGRESS = "li-ext-in-progress";
    const CLASS_ICON_ERROR = "li-ext-error";
    const CLASS_HIDDEN = "hidden";
    const CLASS_INACTIVE = "inactive";

    let addButton, commentButton, tags;

    (function init() {
        listenForURLChange((path) => {
            onChangePage(path)
        });

        onChangePage(location.pathname);
    })();

    // We must know each url change.
    // Decorate window.history.pushState with function
    // that will intercept pushState call
    // and will notify as via the div#li-ext-pushstate `click` event.
    function listenForURLChange(cb) {
        let body = document.getElementsByTagName("body")[0];

        document.addEventListener('onurlchange', (e) => {
            cb(e.detail.path);
        });

        let html = `
        let pushState = window.history.pushState;
        window.history.pushState = function(state) {
            document.dispatchEvent(new CustomEvent('onurlchange', { detail: state }));
            return pushState.apply(window.history, arguments);
        };`;

        let patch = document.createElement("script");
        patch.type = 'text/javascript';
        patch.innerHTML = html;
        body.appendChild(patch);

        // Also we'll catch "back" button.
        window.onpopstate = (e) => { cb(e.state.path) };
    }

    function onChangePage(path) {
        if (!isProfilePage(path)) {
            return;
        }

        resetUI();
        drawUI();
        handleClicks();

        // set icon for user.
        checkProfileAlreadyExists(path).then(profile => {
            if (profile) {
                hideAddButton();
                showCommentButton();
                enableTags(profile.tags);
            } else {
                changeAddButtonClassTo(CLASS_ICON_ADD);
                hideCommentButton()
            }
        }).catch((e) => {
            changeAddButtonClassTo(CLASS_ICON_ERROR);
            console.error(e);
        });
    }

    function isProfilePage(path) {
        return path.substring(0, 4) === "/in/"
    }

    function resetUI() {
        $(`#${CONTAINER_ID}`).remove()
    }

    function drawUI() {
        $(PARENT).append(`
<div id="${CONTAINER_ID}">
    <div class="pv-top-card-v2-section__actions mt2 display-flex">
        <span class="pv-s-profile-actions button-primary-large mr2 mt2 li-ext-btn-container">
            <div id='${ADD_BUTTON_ID}'><a class="${CLASS_ICON_IN_PROGRESS}"></a></div>
        </span>
        <a id='${COMMENT_BUTTON_ID}' class="pv-s-profile-actions button-primary-large mr2 mt2 li-ext-btn-container ${CLASS_HIDDEN}">Comment</a>
    </div>
    
    <div id="${COMMENT_CONTAINER_ID}" class="pv-top-card-v2-section__actions mt2 display-flex ${CLASS_HIDDEN}">
        <textarea disabled>Doesn't implemented yet</textarea>
    </div>
    
    <div id="${TAGS_CONTAINER_ID}" class="${CLASS_HIDDEN}">
        <input id="${TAGS_ID}" type="text" name="tags" placeholder="Add tag" class="tm-input-typeahead" />
    </div>
</div>
`);
        // jquery wrapper for rendered button.
        addButton = $(`#${ADD_BUTTON_ID}`);
        commentButton = $(`#${COMMENT_BUTTON_ID}`);
        tags = $(`#${TAGS_ID}`);
    }

    function checkProfileAlreadyExists(path) {
        return getProfile(getProfileIDFromPath(path));
    }

    function hideAddButton() {
        addButton.parent().addClass(CLASS_HIDDEN)
    }

    function changeAddButtonClassTo(newClass) {
        let button = addButton.find("a");

        button.removeClass(); // Remove all classes.
        button.addClass(CLASS_ICON);
        button.addClass(newClass);
    }

    function showCommentButton() {
        commentButton.removeClass(CLASS_HIDDEN)
    }

    function enableTags(profileTags) {
        tags.tagsManager({
            prefilled: profileTags,
            deleteTagsOnBackspace: false,
        });

        $(`#${TAGS_CONTAINER_ID}`).removeClass(CLASS_HIDDEN);

        tags.on('tm:pushing', (e, tag) => {
            saveTag(getProfileID(), tag).then(() => {
                tags.typeahead('val', ''); // Reset input.
            }).catch((err) => {
                $(this).notifyMe(
                    'top',
                    'error',
                    "Failed to add tag!",
                    `Failed to add tag "${tag}"\nto user "${getProfileID()}".\nReason: ${err}`,
                    200
                );

                tags.tagsManager("popTag")
            })
        });

        tags.on('tm:splicing', (e, tag) => {
            deleteTag(getProfileID(), tag).catch((err) => {
                $(this).notifyMe(
                    'top',
                    'error',
                    "Failed to delete tag!",
                    `Failed to delete tag "${tag}"\nfrom user "${getProfileID()}".\nReason: ${err}`,
                    200
                );

                tags.tagsManager("pushTag", tag)
            })
        });

        let availableTags = new Bloodhound({
            local: ['call later', 'now is not available', 'a', 'ab', 'abc', 'abcd', 'not now', 'test'], // TODO implement.
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            limit: 10
        });

        availableTags.initialize();

        let options = {
            hint: false,
            highlight: true
        };

        tags.typeahead(options, {
            source: availableTags.ttAdapter()
        }).on('typeahead:selected', function (e, d) {
            tags.tagsManager("pushTag", d.name);
        });
    }

    function hideCommentButton() {
        commentButton.addClass(CLASS_HIDDEN)
    }

    // Trim /in/aleksey-motovilov-6942a42b/ to aleksey-motovilov-6942a42b.
    function getProfileIDFromPath(path) {
        return path.match(/\/in\/([^/]+)\/?/)[1]
    }

    // Can be used ONLY when page is fully loaded.
    // During the page load there is safely to get right ID
    // using the URL specified in transit element.
    function getProfileID() {
        return getProfileIDFromPath(location.pathname);
    }

    function handleClicks() {
        addButton.on("click", addProfile);

        commentButton.on("click", () => {
            $(`#${COMMENT_CONTAINER_ID}`).toggleClass(CLASS_HIDDEN)
        })
    }

    function addProfile() {
        let icon = addButton.find(`.${CLASS_ICON}`);

        // Process events only when icon-add is set.
        if (!icon.hasClass(CLASS_ICON_ADD)) {
            return;
        }

        changeAddButtonClassTo(CLASS_ICON_IN_PROGRESS);

        enableExperienceBlock();
        setTimeout(() => {
            sendProfile(parseProfile(), afterSuccessfulProfileSave, profileWasNotSaved);
        }, 100);
    }

    // hack for accessing experience block.
    function enableExperienceBlock() {
        $(document).scrollTop(1000);
    }

    function parseProfile() {
        let lastJobTitle, lastJobCompany;
        let jobs = $('.pv-position-entity');

        if (jobs.length > 0) {
            let job = jobs.first();
            lastJobTitle = job.find('.pv-entity__summary-info h3').text();
            lastJobCompany = job.find('.pv-entity__secondary-title').text();
        }

        return {
            linkedin_id: getProfileID(),
            url: location.href,
            name: $('.pv-top-card-section__name').text(),
            email: getEmail(),
            title: $('.pv-top-card-section__headline').text(),
            last_job_company: lastJobCompany,
            last_job_title: lastJobTitle
        };
    }

    function getEmail() {
        // Open contacts.
        $('a[data-control-name="contact_see_more"]').click();

        let email = $('.ci-email').find('a').text().trim();
        console.info("Email:", email);

        // Close contacts.
        $('[aria-labelledby="pv-contact-info"] > button').click();

        return email
    }

    function getProfile(id) {
        let message = {method: "get_profile", id: id};

        return new Promise(resolve => {
            sendMessage(
                message,
                (response) => {
                    if (response.profile.linkedin_id === id) {
                        resolve(response.profile);
                    } else {
                        throw new Error(`Gotten profile id "${response.profile.linkedin_id}" doesn't match expected id "${id}"`)
                    }
                },
                (errorCode, errorMessage) => {
                    if (errorCode === 404) {
                        resolve(false)
                    } else {
                        throw new Error(`Error during getting profile: [${errorCode}]${errorMessage}`);
                    }
                }
            );
        });
    }

    function sendProfile(profile, onSuccess, onError) {
        let message = {method: "save_profile", profile: profile};
        sendMessage(message, onSuccess, onError);
    }

    function saveTag(profileID, tag) {
        let message = {method: "save_tag", id: profileID, tag: tag};

        return new Promise((resolve, reject) => {
            sendMessage(
                message,
                resolve,
                (errorCode, errorMessage) => {
                    reject(new Error(`Failed to save tag: [${errorCode}]${errorMessage}`));
                }
            );
        });
    }

    function deleteTag(profileID, tag) {
        let message = {method: "delete_tag", id: profileID, tag: tag};

        return new Promise((resolve, reject) => {
            sendMessage(
                message,
                resolve,
                (errorCode, errorMessage) => {
                    reject(new Error(`Failed to delete tag: [${errorCode}]${errorMessage}`));
                }
            );
        });
    }

    function sendMessage(message, onSuccess, onError) {
        chrome.runtime.sendMessage(message, (response) => {
            if (response.success === true) {
                return onSuccess(response);
            }

            onError(response.error_code, response.error_message);
        });
    }

    function afterSuccessfulProfileSave() {
        changeAddButtonClassTo(CLASS_ICON_ADDED);
        makeAddButtonInactive();
        showCommentButton();
        enableTags();
    }

    function makeAddButtonInactive() {
        addButton.parent().addClass(CLASS_INACTIVE)
    }

    function profileWasNotSaved(errorCode, errorMessage) {
        changeAddButtonClassTo(CLASS_ICON_ERROR);
        console.error(`Got error during saving profile: [${errorCode}]${errorMessage}`);
    }
});
